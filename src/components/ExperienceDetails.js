import {
  Button,
  Checkbox,
  Grid,
  InputAdornment,
  Paper,
  TextField,
} from "@material-ui/core";
import { useNavigate } from "react-router-dom";

import {
  ArrowLeft,
  ArrowRight,
  Business,
  Description,
  EventSeat,
  Timelapse,
} from "@material-ui/icons";
import React, { useState } from "react";

export default function ExperienceDetails() {
  const history = useNavigate();
  const [isChecked, setIsChecked] = useState(true);
  const [isCheckedExp, setIsCheckedExp] = useState(true);

  return (
    <div>
      <div className="w-100 d-flex align-items-center  justify-content-center">
        <Grid className="w-75 mt-2 mx-5">
          <Paper style={{ paddingTop: "10px" }}>
            <Paper
              style={{
                margin: " 0 10px",
                textAlign: "center",
                fontSize: "25px",
                fontWeight: "bold",
              }}
            >
              <p>Experience Details</p>
            </Paper>
            <Grid className="mt-3" style={{ marginLeft: "12%" }}>
              <Checkbox
                value="checkedA"
                color="black"
                inputProps={{ "aria-label": "Checkbox A" }}
                onClick={() => setIsChecked(!isChecked)}
              />
              <p style={{ fontSize: "20px", fontWeight: "bold" }}>
                Experience 1
              </p>
            </Grid>
            <Grid
              container
              spacing={4}
              style={{
                display: "flex",
                marginTop: "10px",
                justifyContent: "center",
              }}
            >
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Institute/Organisation *"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Business />
                      </InputAdornment>
                    ),
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isChecked}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  placeholder="Position *"
                  variant="outlined"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <EventSeat />
                      </InputAdornment>
                    ),
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isChecked}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  placeholder="Duration *"
                  variant="outlined"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Timelapse />
                      </InputAdornment>
                    ),
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isChecked}
                />
              </Grid>
            </Grid>
            <Grid className="mt-4" style={{ marginLeft: "13%" }} item xs={12}>
              <TextField
                className="w-100"
                variant="outlined"
                placeholder="Description *"
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <Description />
                    </InputAdornment>
                  ),
                  style: {
                    width: "86%",
                    height: 30,
                  },
                }}
                disabled={isChecked}
              />
            </Grid>
            <hr className="mx-4 my-4" />
            <Grid className="mt-3" style={{ marginLeft: "12%" }}>
              <Checkbox
                value="checkedA"
                color="black"
                inputProps={{ "aria-label": "Checkbox A" }}
                onClick={() => setIsCheckedExp(!isCheckedExp)}
              />
              <p style={{ fontSize: "20px", fontWeight: "bold" }}>
                Experience 2
              </p>
            </Grid>
            <Grid
              container
              spacing={4}
              style={{
                display: "flex",
                marginTop: "10px",
                justifyContent: "center",
              }}
            >
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Institute/Organisation *"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Business />
                      </InputAdornment>
                    ),
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isCheckedExp}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  placeholder="Position *"
                  variant="outlined"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <EventSeat />
                      </InputAdornment>
                    ),
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isCheckedExp}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  placeholder="Duration *"
                  variant="outlined"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Timelapse />
                      </InputAdornment>
                    ),
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isCheckedExp}
                />
              </Grid>
            </Grid>
            <Grid className="mt-4" style={{ marginLeft: "13%" }} item xs={12}>
              <TextField
                className="w-100"
                variant="outlined"
                placeholder="Description *"
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <Description />
                    </InputAdornment>
                  ),
                  style: {
                    width: "86%",
                    height: 30,
                  },
                }}
                disabled={isCheckedExp}
              />
            </Grid>
            <Grid
              container
              spacing={4}
              style={{
                display: "flex",
                marginTop: "20px",
                justifyContent: "center",
              }}
            >
              <Grid>
                <Button
                  style={{ backgroundColor: "#AB2A41", color: "white" }}
                  className="me-5"
                  variant="outlined"
                  startIcon={<ArrowLeft />}
                  onClick={() => history("/projectsDeveloped")}
                >
                  Back
                </Button>
              </Grid>
              <Grid>
                <Button
                  className="ms-5"
                  style={{ backgroundColor: "#EE173E", color: "white" }}
                  variant="outlined"
                  endIcon={<ArrowRight />}
                  onClick={() => history("/extradetails")}
                >
                  Next
                </Button>
              </Grid>
            </Grid>
            <Grid
              style={{
                textAlign: "center",
                paddingBottom: "10px",
                paddingTop: "30px",
              }}
            >
              <p>Page 4</p>
            </Grid>
          </Paper>
        </Grid>
      </div>
    </div>
  );
}
