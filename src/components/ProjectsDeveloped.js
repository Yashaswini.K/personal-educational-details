import {
  Button,
  Grid,
  InputAdornment,
  Paper,
  TextField,
} from "@material-ui/core";
import {
  ArrowLeft,
  ArrowRight,
  Description,
  Link,
  Title,
} from "@material-ui/icons";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

export default function ProjectsDeveloped() {
  const history = useNavigate();
  const [projectData1, setProjectData1] = useState({
    link: "",
    title: "",
    description: "",
  });
  const [projectData2, setProjectData2] = useState({
    link: "",
    title: "",
    description: "",
  });
  const initialError = {
    link: false,
    title: false,
    description: false,
  };
  const [error, setErrors] = useState(initialError);
  const validation = () => {
    const errObj = {
      link: false,
      title: false,
      description: false,
      link1: false,
      title1: false,
      description1: false,
    };
    if (projectData1.title === "") {
      errObj.title = true;
    }
    if (projectData1.link === "") {
      errObj.link = true;
    }
    if (projectData1.description === "") {
      errObj.description = true;
    }
    if (projectData2.title === "") {
      errObj.title1 = true;
    }
    if (projectData2.link === "") {
      errObj.link1 = true;
    }
    if (projectData2.description === "") {
      errObj.description1 = true;
    }
    return errObj
  };
  const handleNext = () => {
    setErrors(validation())
    let err = validation();
    let finalVal = Object.values(err).some((val) => val);
    if (!finalVal) {
      history("/experienceDetails");
    }
  };
  return (
    <div>
      <div className="w-100 d-flex align-items-center  justify-content-center">
        <Grid className="w-75 mt-2 mx-5">
          <Paper style={{ paddingTop: "10px" }}>
            <Paper
              style={{
                margin: " 0 10px",
                textAlign: "center",
                fontSize: "25px",
                fontWeight: "bold",
              }}
            >
              <text>Projects Developed</text>
            </Paper>
            <Grid className="d-flex align-items-center justify-content-center ">
              <p
                className="mt-3"
                style={{ fontSize: "20px", fontWeight: "bold" }}
              >
                Project 1
              </p>
            </Grid>

            <Grid container spacing={2} className="mt-2 ">
              <Grid className="d-flex justify-content-center mt-3" item xs={12}>
                <TextField
                  className="w-50"
                  variant="outlined"
                  placeholder="Title *"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Title />
                      </InputAdornment>
                    ),
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  value={projectData1.title}
                  onChange={(e) =>
                    setProjectData1((prev) => ({
                      ...prev,
                      title: e.target.value,
                    }))
                  }
                />
              </Grid>
              <Grid className="d-flex justify-content-center mt-3" item xs={12}>
                <TextField
                  className="w-50"
                  variant="outlined"
                  placeholder="Link *"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Link />
                      </InputAdornment>
                    ),
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  value={projectData1.link}
                  onChange={(e) =>
                    setProjectData1((prev) => ({
                      ...prev,
                      link: e.target.value,
                    }))
                  }
                />
              </Grid>
              <Grid className="d-flex justify-content-center mt-3" item xs={12}>
                <TextField
                  className="w-50"
                  variant="outlined"
                  placeholder="Description *"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Description />
                      </InputAdornment>
                    ),
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  value={projectData1.description}
                  onChange={(e) =>
                    setProjectData1((prev) => ({
                      ...prev,
                      description: e.target.value,
                    }))
                  }
                />
              </Grid>
            </Grid>
            <hr className="mx-4 my-4" />

            <Grid className="d-flex align-items-center justify-content-center">
              <p className="" style={{ fontSize: "20px", fontWeight: "bold" }}>
                Project 2
              </p>
            </Grid>
            <Grid container spacing={2} className="mt-2 ">
              <Grid className="d-flex justify-content-center mt-3" item xs={12}>
                <TextField
                  className="w-50"
                  variant="outlined"
                  placeholder="Title *"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Title />
                      </InputAdornment>
                    ),
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  value={projectData2.title}
                  onChange={(e) =>
                    setProjectData2((prev) => ({
                      ...prev,
                      title: e.target.value,
                    }))
                  }
                />
              </Grid>
              <Grid className="d-flex justify-content-center mt-3" item xs={12}>
                <TextField
                  className="w-50"
                  variant="outlined"
                  placeholder="Link *"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Link />
                      </InputAdornment>
                    ),
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  value={projectData2.link}
                  onChange={(e) =>
                    setProjectData2((prev) => ({
                      ...prev,
                      link: e.target.value,
                    }))
                  }
                />
              </Grid>
              <Grid className="d-flex justify-content-center mt-3" item xs={12}>
                <TextField
                  className="w-50"
                  variant="outlined"
                  placeholder="Description *"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Description />
                      </InputAdornment>
                    ),
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  value={projectData2.description}
                  onChange={(e) =>
                    setProjectData2((prev) => ({
                      ...prev,
                      description: e.target.value,
                    }))
                  }
                />
              </Grid>
            </Grid>
            <Grid
              container
              spacing={4}
              style={{
                display: "flex",
                marginTop: "10px",
                justifyContent: "center",
              }}
            >
              <Grid>
                <Button
                  style={{ backgroundColor: "#AB2A41", color: "white" }}
                  className="me-5"
                  variant="outlined"
                  startIcon={<ArrowLeft />}
                  onClick={() => history("/educationDetails")}
                >
                  Back
                </Button>
              </Grid>
              <Grid>
                <Button
                  className="ms-5"
                  style={{ backgroundColor: "#EE173E", color: "white" }}
                  variant="outlined"
                  endIcon={<ArrowRight />}
                  onClick={handleNext}
                >
                  Next
                </Button>
              </Grid>
            </Grid>
            <Grid
              style={{
                textAlign: "center",
                paddingBottom: "10px",
                paddingTop: "20px",
              }}
            >
              <p>Page 3</p>
            </Grid>
          </Paper>
        </Grid>
      </div>
    </div>
  );
}
