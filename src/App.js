import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import PersonalDetails from "./components/PersonalDetails";
import EducationDetails from "./components/EducationDetails";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import ProjectsDeveloped from "./components/ProjectsDeveloped";
import ExperienceDetails from "./components/ExperienceDetails";
import ExtraDetails from "./components/ExtraDetails";
import { Menu } from "@material-ui/icons";
function App() {
  return (
    <div>
      <nav className="bg-primary">
        <div className="d-flex justify-content-between mx-2">
          <div>
            <button>
              <span>
                <Menu color="primary" />
              </span>
            </button>
          </div>
          <div>
            <span style={{ color: "white" }}>Resume Builder</span>
          </div>
          <div>
            <span style={{ color: "white" }}>Login</span>
          </div>
        </div>
      </nav>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<PersonalDetails />} />
          <Route path="/educationDetails" element={<EducationDetails />} />
          <Route path="/projectsDeveloped" element={<ProjectsDeveloped />} />
          <Route path="/experienceDetails" element={<ExperienceDetails />} />
          <Route path="/ExtraDetails" element={<ExtraDetails />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
