import {
  Button,
  Grid,
  InputAdornment,
  Paper,
  TextField,
  Typography,
} from "@material-ui/core";
import { ArrowLeft, ArrowRight, Description, School } from "@material-ui/icons";
import { Alert, Snackbar } from "@mui/material";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

export default function EducationDetails() {
  const history = useNavigate();
  const [educationalData, setEducationalData] = useState({
    college: "",
    clgStartDate: "",
    clgEndDate: "",
    clgQuali: "",
    clgDes: "",
    school: "",
    schoolStartDate: "",
    schoolEndDate: "",
    schoolQuli: "",
    schoolDes: "",
  });
  const initialError = {
    college: false,
    clgStartDate: false,
    clgEndDate: false,
    school: false,
    schoolStartDate: false,
    schoolEndDate: false,
    clgQuali: false,
    clgDes: false,
    schoolQuli: false,
    schoolDes: false,
  };
  const [errors, setErrors] = useState(initialError);
  const validation = () => {
    const errObj = {
      college: false,
      clgStartDate: false,
      clgEndDate: false,
      school: false,
      schoolStartDate: false,
      schoolEndDate: false,
      clgQuali: false,
      clgDes: false,
      schoolQuli: false,
      schoolDes: false,
    };
    let clgStrtDate = new Date(educationalData.clgStartDate);
    let clgEnDate = new Date(educationalData.clgEndDate);
    let schoolStrtDate = new Date(educationalData.schoolStartDate);
    let schoolEnDate = new Date(educationalData.schoolEndDate);

    if (educationalData.college === "") {
      errObj.college = true;
    }
    if (educationalData.school === "") {
      errObj.school = true;
    }
    if (educationalData.clgStartDate === "" || !(clgStrtDate < clgEnDate)) {
      errObj.clgStartDate = true;
    }
    if (educationalData.clgEndDate === "" || !(clgEnDate > clgStrtDate)) {
      errObj.clgEndDate = true;
    }
    if (
      educationalData.schoolStartDate === "" ||
      !(schoolStrtDate < schoolEnDate)
    ) {
      errObj.schoolStartDate = true;
    }
    if (
      educationalData.schoolEndDate === "" ||
      !(schoolEnDate > schoolStrtDate)
    ) {
      errObj.schoolEndDate = true;
    }
    if (educationalData.schoolQuli === "") {
      errObj.schoolQuli = true;
    }
    if (educationalData.schoolDes === "") {
      errObj.schoolDes = true;
    }
    if (educationalData.clgQuali === "") {
      errObj.clgQuali = true;
    }
    if (educationalData.clgDes === "") {
      errObj.clgDes = true;
    }
    return errObj;
  };
  const handleNext = () => {
    setErrors(validation());
    let err = validation();
    let finalval = Object.values(err).some((val) => val);
    if (!finalval) {
      history("/projectsDeveloped");
    }
  };
  console.log(educationalData.clgStartDate);
  console.log(educationalData.clgEndDate);

  return (
    <div>
      <div className="w-100 d-flex align-items-center  justify-content-center">
        <Grid className="w-75 mt-2 mx-5">
          <Paper style={{ paddingTop: "10px" }}>
            <Paper
              style={{
                margin: " 0 10px",
                textAlign: "center",
                fontSize: "25px",
                fontWeight: "bold",
              }}
            >
              <p>Education Details</p>
            </Paper>
            <Grid
              container
              spacing={4}
              style={{
                display: "flex",
                marginTop: "10px",
                justifyContent: "center",
              }}
            >
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="College/University *"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <School />
                      </InputAdornment>
                    ),
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  value={educationalData.college}
                  onChange={(e) =>
                    setEducationalData((prev) => ({
                      ...prev,
                      college: e.target.value,
                    }))
                  }
                />
                {errors.college && (
                  <Typography color="error" variant="caption">
                    fieldRequired
                  </Typography>
                )}
              </Grid>
              <Grid item xs={3}>
                <TextField
                  id="date"
                  className="w-100"
                  placeholder="Date"
                  variant="outlined"
                  type="date"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  value={educationalData.clgStartDate}
                  onChange={(e) =>
                    setEducationalData((prev) => ({
                      ...prev,
                      clgStartDate: e.target.value,
                    }))
                  }
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  id="date"
                  className="w-100"
                  placeholder="Date"
                  variant="outlined"
                  type="date"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  value={educationalData.clgEndDate}
                  onChange={(e) =>
                    setEducationalData((prev) => ({
                      ...prev,
                      clgEndDate: e.target.value,
                    }))
                  }
                />
              </Grid>
            </Grid>
            <Grid
              container
              spacing={4}
              style={{
                display: "flex",
                marginTop: "10px",
                justifyContent: "center",
              }}
            >
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Qualification *"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  value={educationalData.clgQuali}
                  onChange={(e) =>
                    setEducationalData((prev) => ({
                      ...prev,
                      clgQuali: e.target.value,
                    }))
                  }
                />
                {errors.clgQuali && (
                  <Typography color="error" variant="caption">
                    fieldRequired
                  </Typography>
                )}
              </Grid>
              <Grid item xs={6}>
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Description *"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Description />
                      </InputAdornment>
                    ),
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  value={educationalData.clgDes}
                  onChange={(e) =>
                    setEducationalData((prev) => ({
                      ...prev,
                      clgDes: e.target.value,
                    }))
                  }
                />
                {errors.clgDes && (
                  <Typography color="error" variant="caption">
                    fieldRequired
                  </Typography>
                )}
              </Grid>
            </Grid>
            <hr className="mx-4 my-4" />
            <Grid
              container
              spacing={4}
              style={{
                display: "flex",
                justifyContent: "center",
              }}
            >
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="School *"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <School />
                      </InputAdornment>
                    ),
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  value={educationalData.school}
                  onChange={(e) =>
                    setEducationalData((prev) => ({
                      ...prev,
                      school: e.target.value,
                    }))
                  }
                />
                {errors.school && (
                  <Typography color="error" variant="caption">
                    fieldRequired
                  </Typography>
                )}
              </Grid>
              <Grid item xs={3}>
                <TextField
                  id="date"
                  className="w-100"
                  placeholder="Date"
                  variant="outlined"
                  type="date"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  value={educationalData.schoolStartDate}
                  onChange={(e) =>
                    setEducationalData((prev) => ({
                      ...prev,
                      schoolStartDate: e.target.value,
                    }))
                  }
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  id="date"
                  className="w-100"
                  placeholder="Date"
                  variant="outlined"
                  type="date"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  value={educationalData.schoolEndDate}
                  onChange={(e) =>
                    setEducationalData((prev) => ({
                      ...prev,
                      schoolEndDate: e.target.value,
                    }))
                  }
                />
              </Grid>
            </Grid>
            <Grid
              container
              spacing={4}
              style={{
                display: "flex",
                marginTop: "10px",
                justifyContent: "center",
              }}
            >
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Qualification *"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  value={educationalData.schoolQuli}
                  onChange={(e) =>
                    setEducationalData((prev) => ({
                      ...prev,
                      schoolQuli: e.target.value,
                    }))
                  }
                />
                {errors.schoolQuli && (
                  <Typography color="error" variant="caption">
                    fieldRequired
                  </Typography>
                )}
              </Grid>
              <Grid item xs={6}>
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Description *"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Description />
                      </InputAdornment>
                    ),
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  value={educationalData.schoolDes}
                  onChange={(e) =>
                    setEducationalData((prev) => ({
                      ...prev,
                      schoolDes: e.target.value,
                    }))
                  }
                />
                {errors.schoolDes && (
                  <Typography color="error" variant="caption">
                    fieldRequired
                  </Typography>
                )}
              </Grid>
            </Grid>
            <Grid
              container
              spacing={4}
              style={{
                display: "flex",
                marginTop: "10px",
                justifyContent: "center",
              }}
            >
              <Grid>
                <Button
                  style={{ backgroundColor: "#AB2A41", color: "white" }}
                  className="me-5"
                  variant="outlined"
                  startIcon={<ArrowLeft />}
                  onClick={() => history("/")}
                >
                  Back
                </Button>
              </Grid>
              <Grid>
                <Button
                  className="ms-5"
                  style={{ backgroundColor: "#EE173E", color: "white" }}
                  variant="outlined"
                  endIcon={<ArrowRight />}
                  onClick={handleNext}
                >
                  Next
                </Button>
              </Grid>
            </Grid>
            <Grid
              style={{
                textAlign: "center",
                paddingBottom: "10px",
                paddingTop: "20px",
              }}
            >
              <p>Page 2</p>
            </Grid>
            {errors.clgStartDate && (
              <Alert
                severity="error"
                style={{
                  marginLeft: "20%",
                  marginRight: "20%",
                  color: "white",
                  backgroundColor: "#DB0505",
                }}
                onClose={() =>
                  setTimeout(() => {
                    setErrors((prev) => ({
                      ...prev,
                      clgStartDate: false,
                    }));
                  }, 100)
                }
              >

                College Start Date required and should be less than End Date
              </Alert>
            )}
            {errors.schoolStartDate && (
              <Alert
                onClose={() =>
                  setTimeout(() => {
                    setErrors((prev) => ({
                      ...prev,
                      schoolStartDate: false,
                    }));
                  }, 100)
                }
                severity="error"
                style={{
                  marginLeft: "20%",
                  marginRight: "20%",
                  color: "white",
                  backgroundColor: "#DB0505",
                }}
              >
                School Start Date required and should be less than End Date
              </Alert>
            )}
          </Paper>
        </Grid>
      </div>
    </div>
  );
}
