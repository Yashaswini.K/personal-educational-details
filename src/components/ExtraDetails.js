import { Button, Checkbox, Grid, Paper, TextField } from "@material-ui/core";
import { ArrowLeft, ArrowRight, GetApp } from "@material-ui/icons";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

export default function ExtraDetails() {
  const navigate = useNavigate();
  const [isChecked, setIsChecked] = useState(true);
  const [isCheckedInt, setIsCheckedInt] = useState(true);

  return (
    <div>
      <div className="w-100 d-flex align-items-center  justify-content-center">
        <Grid className="w-75 mt-2 mx-5">
          <Paper style={{ paddingTop: "10px" }}>
            <Paper
              style={{
                margin: " 0 10px",
                textAlign: "center",
                fontSize: "25px",
                fontWeight: "bold",
              }}
            >
              <p>Extra Details</p>
            </Paper>
            <Grid className="mt-3" style={{ marginLeft: "12%" }}>
              <Checkbox
                value="checkedA"
                color="black"
                inputProps={{ "aria-label": "Checkbox A" }}
                onClick={() => setIsChecked(!isChecked)}
              />
              <p style={{ fontSize: "20px", fontWeight: "bold" }}>
                Skills/Languages
              </p>
            </Grid>
            <Grid
              container
              spacing={4}
              style={{
                display: "flex",
                marginTop: "10px",
                justifyContent: "center",
              }}
            >
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Skill 1"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isChecked}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  placeholder="Skill 2"
                  variant="outlined"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isChecked}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  placeholder="Skill 3"
                  variant="outlined"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isChecked}
                />
              </Grid>
            </Grid>
            <Grid
              container
              spacing={4}
              style={{
                display: "flex",
                marginTop: "10px",
                justifyContent: "center",
              }}
            >
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Skill 4"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isChecked}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  placeholder="Skill 5"
                  variant="outlined"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isChecked}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  placeholder="Skill 6"
                  variant="outlined"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isChecked}
                />
              </Grid>
            </Grid>
            <Grid className="mt-3" style={{ marginLeft: "12%" }}>
              <Checkbox
                value="checkedA"
                color="black"
                inputProps={{ "aria-label": "Checkbox A" }}
                onClick={() => setIsCheckedInt(!isCheckedInt)}
              />
              <p style={{ fontSize: "20px", fontWeight: "bold" }}>Interest</p>
            </Grid>
            <Grid
              container
              spacing={4}
              style={{
                display: "flex",
                marginTop: "10px",
                justifyContent: "center",
              }}
            >
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Interest 1"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isCheckedInt}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  placeholder="Interest 2"
                  variant="outlined"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isCheckedInt}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  placeholder="Interest 3"
                  variant="outlined"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isCheckedInt}
                />
              </Grid>
            </Grid>
            <Grid
              container
              spacing={4}
              style={{
                display: "flex",
                marginTop: "10px",
                justifyContent: "center",
              }}
            >
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Interest 4"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isCheckedInt}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  placeholder="Interest 5"
                  variant="outlined"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isCheckedInt}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  className="w-100"
                  placeholder="Interest 6"
                  variant="outlined"
                  InputProps={{
                    style: {
                      width: "100%",
                      height: 30,
                    },
                  }}
                  disabled={isCheckedInt}
                />
              </Grid>
            </Grid>
            <Grid
              container
              spacing={4}
              style={{
                display: "flex",
                marginTop: "20px",
                justifyContent: "center",
              }}
            >
              <Grid>
                <Button
                  style={{ backgroundColor: "#AB2A41", color: "white" }}
                  className="me-5"
                  variant="outlined"
                  startIcon={<ArrowLeft />}
                  onClick={() => navigate("/ExperienceDetails")}
                >
                  Back
                </Button>
              </Grid>
              <Grid>
                <Button
                  disabled={true}
                  className="ms-5"
                  variant="outlined"
                  endIcon={<ArrowRight />}
                >
                  Next
                </Button>
              </Grid>
            </Grid>
            <Grid
              style={{
                textAlign: "center",
                paddingBottom: "10px",
                paddingTop: "40px",
              }}
            >
              <Button
                style={{ backgroundColor: "#5D5DE3", color: "white" }}
                className="me-5"
                variant="outlined"
                endIcon={<GetApp />}
              >
                Download Resume
              </Button>
            </Grid>
          </Paper>
        </Grid>
      </div>
    </div>
  );
}
