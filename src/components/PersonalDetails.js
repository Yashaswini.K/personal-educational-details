import {
  Button,
  Grid,
  InputAdornment,
  Paper,
  TextField,
  Typography,
} from "@material-ui/core";
import React, { useState } from "react";
import {
  ArrowLeft,
  ArrowRight,
  Facebook,
  GitHub,
  Instagram,
  Language,
  LinkedIn,
  Mail,
  Phone,
  Twitter,
} from "@material-ui/icons";
import { useNavigate } from "react-router-dom";
export default function PersonalDetails() {
  const [personalData, setPersonalData] = useState({
    firstName: "",
    latName: "",
    email: "",
    phoneNumber: "",
    website: "",
    linkedIn: "",
    facebook: "",
    instagram: "",
    twitter: "",
    github: "",
  });
  const initialError = {
    firstName: false,
    lastName: false,
    email: false,
    phoneNumber: false,
    website: false,
    linkedIn: false,
    facebook: false,
    instagram: false,
    twitter: false,
    github: false,
  };
  const [errors, setErrors] = useState(initialError);
  const history = useNavigate();

  const validate = () => {
    const errorsObj = {
      firstName: false,
      lastName: false,
      email: false,
      phoneNumber: false,
      website: false,
      linkedIn: false,
      facebook: false,
      instagram: false,
      twitter: false,
      github: false,
    };
    const nameError = personalData.firstName ? personalData.firstName : "";
    const lastError = personalData.latName ? personalData.latName : "";
    const emailError = personalData.email ? personalData.email : "";
    const phoneError = personalData.phoneNumber ? personalData.phoneNumber : "";
    const websiteError = personalData.website ? personalData.website : "";
    const linkedInError = personalData.linkedIn ? personalData.linkedIn : "";
    const facebookError = personalData.facebook ? personalData.facebook : "";
    const instaError = personalData.instagram ? personalData.instagram : "";
    const gitError = personalData.github ? personalData.github : "";
    const twitterErr = personalData.twitter ? personalData.twitter : "";
    if (nameError === "" || !/^([^0-9]*)$/.test(personalData.firstName)) {
      errorsObj.firstName = true;
    }
    if (lastError === "" || !/^([^0-9]*)$/.test(personalData.latName)) {
      errorsObj.lastName = true;
    }
    if (
      emailError === "" ||
      !/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        personalData.email
      )
    ) {
      errorsObj.email = true;
    }
    if (
      phoneError === "" ||
      personalData.phoneNumber.length < 10 ||
      personalData.phoneNumber.length > 10
    ) {
      errorsObj.phoneNumber = true;
    }
    if (websiteError === "") {
      errorsObj.website = true;
    }
    if (facebookError === "") {
      errorsObj.facebook = true;
    }
    if (gitError === "") {
      errorsObj.github = true;
    }
    if (linkedInError === "") {
      errorsObj.linkedIn = true;
    }
    if (instaError === "") {
      errorsObj.instagram = true;
    }
    if (twitterErr === "") {
      errorsObj.twitter = true;
    }
    return errorsObj;
  };
  const handleNext = () => {
    setErrors(validate());
    let err = validate();
    let finalvali = Object.values(err).some((val) => val);
    if (!finalvali) {
      history("/educationDetails");
    }
  };

  return (
    <div>
      <Grid container>
        <Grid item xs={9} className=" m-auto mt-2">
          <Paper>
            <Grid className="w-90  mt-2 mx-1">
              <Paper
                style={{
                  margin: "0 10px",
                  textAlign: "center",
                  fontSize: "25px",
                  fontWeight: "bold",
                }}
              >
                <p>Personal Details</p>
              </Paper>
            </Grid>

            <Grid
              style={{
                marginTop: "30px",
                display: "flex",
                justifyContent: "space-between",
                marginLeft: "20%",
                marginRight: "20%",
              }}
            >
              <Grid className=" d-flex flex-column align-items-center justify-content-center">
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="First Name *"
                  InputProps={{
                    style: {
                      width: 300,
                      height: 30,
                    },
                  }}
                  value={personalData.firstName}
                  onChange={(e) =>
                    setPersonalData((prev) => ({
                      ...prev,
                      firstName: e.target.value,
                    }))
                  }
                />
                {errors.firstName && (
                  <Typography
                    color="error"
                    variant="caption"
                    style={{ marginLeft: "-70%" }}
                  >
                    fieldRequired
                  </Typography>
                )}
              </Grid>

              <Grid className="align-items-center ">
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="LastName *"
                  InputProps={{
                    style: {
                      width: 300,
                      height: 30,
                    },
                  }}
                  value={personalData.latName}
                  onChange={(e) =>
                    setPersonalData((prev) => ({
                      ...prev,
                      latName: e.target.value,
                    }))
                  }
                />
                <Grid>
                  {errors.lastName && (
                    <Typography color="error" variant="caption">
                      fieldRequired
                    </Typography>
                  )}
                </Grid>
              </Grid>
            </Grid>
            <Grid
              style={{
                marginTop: "30px",
                display: "flex",
                marginLeft: "20%",
                marginRight: "20%",
                justifyContent: "space-between",
              }}
            >
              <Grid className=" align-items-center justify-content-center">
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Email *"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Mail />
                      </InputAdornment>
                    ),
                    style: {
                      width: 300,
                      height: 30,
                    },
                  }}
                  value={personalData.email}
                  onChange={(e) =>
                    setPersonalData((prev) => ({
                      ...prev,
                      email: e.target.value,
                    }))
                  }
                />
                <Grid>
                  {errors.email && (
                    <Typography color="error" variant="caption">
                      fieldRequired
                    </Typography>
                  )}
                </Grid>
              </Grid>
              <Grid className="  align-items-center justify-content-center">
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Phone Number"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Phone />
                      </InputAdornment>
                    ),
                    style: {
                      width: 300,
                      height: 30,
                    },
                  }}
                  value={personalData.phoneNumber}
                  onChange={(e) =>
                    setPersonalData((prev) => ({
                      ...prev,
                      phoneNumber: e.target.value,
                    }))
                  }
                  type="number"
                />
                <Grid>
                  {errors.phoneNumber && (
                    <Typography color="error" variant="caption">
                      fieldRequired
                    </Typography>
                  )}
                </Grid>
              </Grid>
            </Grid>
            <Grid
              style={{
                marginTop: "30px",
                display: "flex",
                justifyContent: "space-between",
                marginLeft: "20%",
                marginRight: "20%",
              }}
            >
              <Grid className=" align-items-center justify-content-center">
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Your Website"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Language />
                      </InputAdornment>
                    ),
                    style: {
                      width: 300,
                      height: 30,
                    },
                  }}
                  value={personalData.website}
                  onChange={(e) =>
                    setPersonalData((prev) => ({
                      ...prev,
                      website: e.target.value,
                    }))
                  }
                />
                <Grid>
                  {errors.website && (
                    <Typography color="error" variant="caption">
                      fieldRequired
                    </Typography>
                  )}
                </Grid>
              </Grid>
              <Grid className=" align-items-center justify-content-center">
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Github"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <GitHub />
                      </InputAdornment>
                    ),
                    style: {
                      width: 300,
                      height: 30,
                    },
                  }}
                  value={personalData.github}
                  onChange={(e) =>
                    setPersonalData((prev) => ({
                      ...prev,
                      github: e.target.value,
                    }))
                  }
                />
                <Grid>
                  {errors.github && (
                    <Typography color="error" variant="caption">
                      fieldRequired
                    </Typography>
                  )}
                </Grid>
              </Grid>
            </Grid>
            <Grid
              style={{
                marginTop: "30px",
                display: "flex",
                justifyContent: "space-between",
                marginLeft: "20%",
                marginRight: "20%",
              }}
            >
              <Grid className="align-items-center justify-content-center">
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Linked In"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <LinkedIn />
                      </InputAdornment>
                    ),
                    style: {
                      width: 300,
                      height: 30,
                    },
                  }}
                  value={personalData.linkedIn}
                  onChange={(e) =>
                    setPersonalData((prev) => ({
                      ...prev,
                      linkedIn: e.target.value,
                    }))
                  }
                />
                <Grid>
                  {errors.linkedIn && (
                    <Typography color="error" variant="caption">
                      fieldRequired
                    </Typography>
                  )}
                </Grid>
              </Grid>
              <Grid className=" align-items-center justify-content-center">
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Twitter"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Twitter />
                      </InputAdornment>
                    ),
                    style: {
                      width: 300,
                      height: 30,
                    },
                  }}
                  value={personalData.twitter}
                  onChange={(e) =>
                    setPersonalData((prev) => ({
                      ...prev,
                      twitter: e.target.value,
                    }))
                  }
                />
                 <Grid>
                  {errors.twitter && (
                    <Typography color="error" variant="caption">
                      fieldRequired
                    </Typography>
                  )}
                </Grid>
              </Grid>
            </Grid>
            <Grid
              style={{
                marginTop: "30px",
                display: "flex",
                justifyContent: "space-between",
                marginLeft: "20%",
                marginRight: "20%",
              }}
            >
              <Grid className="  align-items-center justify-content-center">
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Facebook"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Facebook />
                      </InputAdornment>
                    ),
                    style: {
                      width: 300,
                      height: 30,
                    },
                  }}
                  value={personalData.facebook}
                  onChange={(e) =>
                    setPersonalData((prev) => ({
                      ...prev,
                      facebook: e.target.value,
                    }))
                  }
                />
                 <Grid>
                  {errors.facebook && (
                    <Typography color="error" variant="caption">
                      fieldRequired
                    </Typography>
                  )}
                </Grid>
              </Grid>
              <Grid className=" align-items-center justify-content-center">
                <TextField
                  className="w-100"
                  variant="outlined"
                  placeholder="Instagram"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Instagram />
                      </InputAdornment>
                    ),
                    style: {
                      width: 300,
                      height: 30,
                    },
                  }}
                  value={personalData.instagram}
                  onChange={(e) =>
                    setPersonalData((prev) => ({
                      ...prev,
                      instagram: e.target.value,
                    }))
                  }
                />
                 <Grid>
                  {errors.instagram && (
                    <Typography color="error" variant="caption">
                      fieldRequired
                    </Typography>
                  )}
                </Grid>
              </Grid>
            </Grid>
            <Grid
              style={{
                display: "flex",
                marginTop: "10px",
                justifyContent: "space-evenly",
              }}
            >
              <Grid>
                <Button
                  variant="outlined"
                  disabled={true}
                  startIcon={<ArrowLeft />}
                >
                  Back
                </Button>
              </Grid>
              <Grid>
                <Button
                  style={{ backgroundColor: "#EE173E", color: "white" }}
                  variant="outlined"
                  endIcon={<ArrowRight />}
                  onClick={handleNext}
                >
                  Next
                </Button>
              </Grid>
            </Grid>
            <Grid style={{ textAlign: "center", paddingBottom: "10px" }}>
              <p>Page 1</p>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
